let axios = require('axios')
let xmljs = require('xml-js')

module.exports = {
	desc: 'Searches posts in rule34.xxx website.',
	nsfw: true,
	aliases: ['rule34'],
	callback: async function(msg,args,line) {
		let res = await axios.get('http://rule34.xxx/index.php?page=dapi&s=post&q=index&limit=100&tags=' + encodeURIComponent(line))
		
		let data = JSON.parse(xmljs.xml2json(res.data, { compact: true }))

		if(data.posts._attributes.count == 0) return msg.reply('couldn\'t find anything');
		
		let newa = []
		
		for (let post of data.posts.post) {
			post = post._attributes
			newa.push({
				desc: "`" + post.tags + "`",
				image: post.file_url
			})
		}

		let FUCK = new this.ReactionList(this, msg, "Rule34: search results" + (line == "" ? "" : " for `" + line + "`"), newa) 
	}
}
