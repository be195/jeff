module.exports = {
	desc: 'Does Twitter captions. Usage: `;caption <url|string> [string]`',
	callback: async function(msg,args,line) {
		if(this.utils.hasEmotes(line)) {
			let em = this.utils.buildErrEmbed("⁉ Sorry, this command does not support emotes in captions.")
			return msg.channel.send({ embed: em })
		}

		let link = await this.utils.fetchImageFromMessage(msg,line)
		let text = line

		if(!link) {
			let em = this.utils.buildErrEmbed("⁉ No link provided.")
			return msg.channel.send({ embed: em })
		}

		if(this.utils.getUrls(text).length > 0) {
			let _args = args
			_args.shift()
			text = _args.join(' ')
		}

		link = link[0]

		if(text.trim() == "") {
			let em = this.utils.buildErrEmbed("⁉ No caption provided.")
			return msg.channel.send({ embed: em })
		}

		let Image = await this.canvas.loadImage(link)
		let canvas = this.canvas.createCanvas(Image.width, 0)
		let ctx = canvas.getContext('2d')
		ctx.font = '32px HelveticaNeue';

		let sizeall = 0
		let lines = [];
		for(let txt of text.split(' ')) {
			let mes = ctx.measureText(txt)
			sizeall += mes.width + 16
			
			let line = Math.floor(sizeall / Image.width)
			if(!lines[line]) lines[line] = "";
			lines[line] += " " + txt
		}
		
		let b = lines.length * 32 + lines.length * 8 + 16
		canvas.height = b + Image.height
		ctx.fillStyle = "white";
		ctx.fillRect(0,0,Image.width,canvas.height)
		ctx.drawImage(Image,0,b)
		ctx.fillStyle = "black";
		
		lines.forEach((line,a) => ctx.fillText(line,0,(a+1) * 32 + (a+1) * 8));

		msg.channel.send({
			files: [{
				attachment: canvas.createPNGStream(),
				name: 'twitter.png'
			}]
		})
	}
}