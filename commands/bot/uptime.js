module.exports = {
	desc: 'Displays uptime for the bot.',
	callback: async function(msg,args,line) {
		const time = Math.round((Date.now() - this.startedAt) / 2);
		let x = this.prettyMs(time);
		
		msg.channel.send(`:rocket: Uptime: ${x}`);
	}
}