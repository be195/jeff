let controls = [
	"◀",
	"▶",
	"⏹"
]

class ReactionList {
	constructor(bot, msg, title, array) {
		this.bot = bot;
		this.author = msg.author;
		this.origMsg = msg;
		this.array = array
		this.currentPage = 0;

		this.embed = new bot.lib.MessageEmbed();
		this.embed.setTitle(title)
		this.embed.setColor(0x09a576)
		this.embed.setAuthor(this.author.tag, this.author.avatarURL({ format: 'png', size: 64 }))
		this.embed.setDescription("hold on...")

		this.Init()
	}

	async onReact(react) {
		if(react.emoji.name == "◀" || react.emoji.name == "▶") {
			let val = (react.emoji.name == "▶") ? 1 : -1;
			this.currentPage += val
			if(!this.array[this.currentPage]) this.currentPage -= val;

			this.updateEmbed()
		} if(react.emoji.name == "⏹") {
			this.reactCollector.stop("request by user")
			this.msg.delete();
			this.msg.channel.send(`<@${this.author.id}>, removed message!`)
			return
		}

		await react.users.remove(this.author)
	}

	async Init() {
		let a = this

		this.msg = await this.origMsg.channel.send({ embed: this.embed })
		this.reactCollector = this.msg.createReactionCollector((r,user) => controls.includes(r.emoji.name) && user.id == this.author.id)
		this.reactCollector.on('collect', r => this.onReact.call(a,r))

		for(let c of controls) {
			await this.msg.react(c)
		}

		this.updateEmbed()
	}

	updateEmbed(edit = true) {
		let res = this.array[this.currentPage]
		if(!res) return;

		this.embed.setDescription(res.desc)
		this.embed.setImage(res.image)
		this.embed.setFooter(`results ${this.currentPage + 1}/${this.array.length}`)
		if(edit) this.msg.edit({ embed:this.embed })
	}
}

module.exports = ReactionList;